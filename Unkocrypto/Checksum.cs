﻿using System;

namespace Unkocrypto
{
    public interface Checksum
    {
        void Reset();
        void Update(int b);
        long Value
        {
            get;
        }
    }
}