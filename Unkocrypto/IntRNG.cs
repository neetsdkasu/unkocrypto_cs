﻿using System;

namespace Unkocrypto
{
    public interface IntRNG
    {
        int NextInt();
    }
}