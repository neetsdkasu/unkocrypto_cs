﻿using System;

namespace Unkocrypto
{
    public sealed class CryptoException : Exception
    {
        public enum Cause
        {
            Unknown,
            InvalidCount,
            InvalidData,
            InvalidChecksum,
            InvalidDataSize,
        }

        static string MakeCauseMessage(Cause cause)
        {
            switch (cause)
            {
                case Cause.Unknown:
                    return "UNKNOWN";
                case Cause.InvalidCount:
                    return "INVALID COUNT";
                case Cause.InvalidData:
                    return "INVALID DATA";
                case Cause.InvalidChecksum:
                    return "INVALID CHECKSUM";
                case Cause.InvalidDataSize:
                    return "INVALID DATASIZE";
                default:
                    return "UNKNOWN (" + cause + ")";
            }
        }

        public CryptoException(Cause cause) : this(MakeCauseMessage(cause))
        {
        }

        public CryptoException() : this(Cause.Unknown)
        {
        }

        public CryptoException(string message)
            : base(message)
        {
        }

        public CryptoException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}