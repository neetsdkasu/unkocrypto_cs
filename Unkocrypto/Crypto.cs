﻿using System;
using System.IO;
using System.Net;

namespace Unkocrypto
{
    public static class Crypto
    {
        public const int MinBlockSize = 32;
        public const int MaxBlockSize = 1 << 20;
        public const int MetaSize = 4 + 8;
        private const int Byte = 0x100;
        private const int Mask = 0x0FF;

        private static int NextInt(IntRNG rand, int bound)
        {
            if ((bound & -bound) == bound)
            {
                uint tmp = unchecked((uint)rand.NextInt()) >> 1;
                return unchecked((int)(((long)bound * (long)tmp) >> 31));
            }
            int bits, val;
            do
            {
                bits = unchecked((int)((uint)rand.NextInt() >> 1));
                val = bits % bound;
            } while (unchecked(bits - val + (bound - 1)) < 0);
            return val;
        }

        public static int Decrypt(int blockSize, Checksum checksum, IntRNG rand, Stream src, Stream dst)
        {
            if (blockSize < MinBlockSize || MaxBlockSize < blockSize)
            {
                var msg = $"must be between {MinBlockSize} and {MaxBlockSize}";
                throw new ArgumentOutOfRangeException("blockSize", msg);
            }
            if (checksum == null)
            {
                throw new ArgumentNullException("checksum");
            }
            if (rand == null)
            {
                throw new ArgumentNullException("rand");
            }
            if (src == null)
            {
                throw new ArgumentNullException("src");
            }
            if (!src.CanRead)
            {
                throw new ArgumentException("must be readable", "src");
            }
            if (dst == null)
            {
                throw new ArgumentNullException("dst");
            }
            if (!dst.CanWrite)
            {
                throw new ArgumentException("must be writable", "dst");
            }
            int dataSize = blockSize - MetaSize;
            int[] mask = new int[blockSize];
            int[] indexes = new int[blockSize];
            byte[] data = new byte[blockSize];
            int len = 0;
            int onebyte = src.ReadByte();
            if (onebyte < 0)
            {
                throw new CryptoException(CryptoException.Cause.InvalidDataSize);
            }
            while (onebyte >= 0)
            {
                for (int i = 0; i < blockSize; i++)
                {
                    mask[i] = NextInt(rand, Byte);
                    indexes[i] = i;
                }
                for (int i = 0; i < blockSize; i++)
                {
                    int j = NextInt(rand, blockSize - i) + i;
                    int tmp = indexes[i];
                    indexes[i] = indexes[j];
                    indexes[j] = tmp;
                }
                for (int i = 0; i < blockSize; i++)
                {
                    if (onebyte < 0)
                    {
                        throw new CryptoException(CryptoException.Cause.InvalidDataSize);
                    }
                    int j = indexes[i];
                    int b = Mask & onebyte;
                    b ^= Mask & mask[j];
                    data[j] = unchecked((byte)b);
                    onebyte = src.ReadByte();
                }
                int count = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(data, dataSize));
                long code = IPAddress.NetworkToHostOrder(BitConverter.ToInt64(data, dataSize + 4));
                if (count < 0 || (count == 0 && onebyte >= 0) || dataSize < count)
                {
                    throw new CryptoException(CryptoException.Cause.InvalidCount);
                }
                len += count;
                checksum.Reset();
                for (int i = 0; i < dataSize; i++)
                {
                    if (i < count)
                    {
                        dst.WriteByte(data[i]);
                        checksum.Update(Mask & (int)data[i]);
                    }
                    else if (data[i] != 0)
                    {
                        throw new CryptoException(CryptoException.Cause.InvalidData);
                    }
                }
                if (code != checksum.Value)
                {
                    throw new CryptoException(CryptoException.Cause.InvalidChecksum);
                }
            }
            return len;
        }

        public static int Encrypt(int blockSize, Checksum checksum, IntRNG rand, Stream src, Stream dst)
        {
            if (blockSize < MinBlockSize || MaxBlockSize < blockSize)
            {
                var msg = $"must be between {MinBlockSize} and {MaxBlockSize}";
                throw new ArgumentOutOfRangeException("blockSize", msg);
            }
            if (checksum == null)
            {
                throw new ArgumentNullException("checksum");
            }
            if (rand == null)
            {
                throw new ArgumentNullException("rand");
            }
            if (src == null)
            {
                throw new ArgumentNullException("src");
            }
            if (!src.CanRead)
            {
                throw new ArgumentException("must be readable", "src");
            }
            if (dst == null)
            {
                throw new ArgumentNullException("dst");
            }
            if (!dst.CanWrite)
            {
                throw new ArgumentException("must be writable", "dst");
            }
            int dataSize = blockSize - MetaSize;
            byte[] memory = new byte[blockSize];
            int len = 0;
            int onebyte = src.ReadByte();
            do
            {
                len += blockSize;
                checksum.Reset();
                int count = 0;
                for (; count < dataSize; count++)
                {
                    if (onebyte < 0)
                    {
                        break;
                    }
                    int b = Mask & onebyte;
                    checksum.Update(b);
                    b ^= Mask & NextInt(rand, Byte);
                    memory[count] = unchecked((byte)b);
                    onebyte = src.ReadByte();
                }
                for (int i = count; i < dataSize; i++)
                {
                    int b = Mask & NextInt(rand, Byte);
                    memory[i] = unchecked((byte)b);
                }
                Array.Copy(BitConverter.GetBytes(IPAddress.HostToNetworkOrder(count)), 0, memory, dataSize, 4);
                Array.Copy(BitConverter.GetBytes(IPAddress.HostToNetworkOrder(checksum.Value)), 0, memory, dataSize + 4, 8);
                for (int i = dataSize; i < memory.Length; i++)
                {
                    int b = Mask & (int)memory[i];
                    b ^= Mask & NextInt(rand, Byte);
                    memory[i] = unchecked((byte)b);
                }
                for (int i = 0; i < memory.Length; i++)
                {
                    int j = NextInt(rand, memory.Length - i) + i;
                    byte tmp = memory[i];
                    memory[i] = memory[j];
                    memory[j] = tmp;
                }
                dst.Write(memory, 0, memory.Length);
            } while (onebyte >= 0);
            return len;
        }
    }
}