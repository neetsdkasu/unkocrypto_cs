namespace UnkocryptoTest
{
    public sealed class JavaRandom : Unkocrypto.IntRNG
    {
        private long _seed = 0L;

        public JavaRandom(long seed)
        {
            SetSeed(seed);
        }

        public void SetSeed(long seed)
        {
            _seed = (seed ^ 0x5_DEEC_E66DL) & ((1L << 48) - 1L);
        }

        private int Next(int bits)
        {
            _seed = unchecked(_seed * 0x5_DEEC_E66DL + 0xBL) & ((1L << 48) - 1L);
            return unchecked((int)(_seed >> (48 - bits)));
        }

        public int NextInt()
        {
            return Next(32);
        }
    }
}