using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Unkocrypto;

namespace UnkocryptoTest
{
    [TestClass]
    public class UnitTest1
    {
        private const long Seed = 1_2345_6789L;

        private static readonly byte[] s_data = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        private static readonly byte[] s_secretSrc = new byte[] {
            0xa2, 0xfc, 0x45, 0x90, 0x64, 0x80, 0x77, 0x46, 0x3f, 0x7e, 0x1d, 0x7c, 0x64, 0xfe,
            0x5c, 0x98, 0x7a, 0x00, 0x79, 0xa8, 0x64, 0xf2, 0x7d, 0xc1, 0xe3, 0x66, 0x31, 0x31,
            0x1e, 0x62, 0xb6, 0x04,
        };

        [TestMethod]
        public void TestDecrypt()
        {
            int blockSize = Crypto.MinBlockSize;

            Checksum checksum = new CRC32();
            IntRNG rng = new JavaRandom(Seed);
            MemoryStream src = new MemoryStream(s_secretSrc);
            MemoryStream dst = new MemoryStream();

            int len = Crypto.Decrypt(blockSize, checksum, rng, src, dst);

            Assert.AreEqual(s_data.Length, len);
            CollectionAssert.AreEqual(s_data, dst.ToArray());
        }

        [TestMethod]
        public void TestEncrypt()
        {
            int blockSize = Crypto.MinBlockSize;

            Checksum checksum = new CRC32();
            IntRNG rng = new JavaRandom(Seed);
            MemoryStream src = new MemoryStream(s_data);
            MemoryStream dst = new MemoryStream();

            int len = Crypto.Encrypt(blockSize, checksum, rng, src, dst);

            Assert.AreEqual(s_secretSrc.Length, len);
            CollectionAssert.AreEqual(s_secretSrc, dst.ToArray());
        }
    }
}